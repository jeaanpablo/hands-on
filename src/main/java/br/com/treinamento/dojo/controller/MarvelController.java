package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import br.com.treinamento.model.*;
import br.com.treinamento.model.Character;
import br.com.treinamento.facade.CharactersFacade;
import br.com.treinamento.facade.ComicFacade;

@RestController
public class MarvelController {
	
	CharactersFacade characterFacade;
	ComicFacade comicFacade;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/getComicsByCharacter")
	public @ResponseBody List<Comic> getComicByCharacter(
			@RequestParam(value = "character_name") String characterName)
	{
		characterFacade = new CharactersFacade();
		comicFacade = new ComicFacade();
		List<Character> list = characterFacade.getAllCharacters();
		List<Comic> listComic = new ArrayList<Comic>();
		
		if(list.isEmpty() == Boolean.FALSE)
		{
			Character character = characterFacade.getCharacterByString(characterName, list);
			
			if(character != null)
			{
				listComic = comicFacade.getComicsByCharacter(character.getId());
			}
		}
		
		
		return listComic;
	}
	
	
	
}
