package br.com.treinamento.facade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.treinamento.model.Character;
import br.com.treinamento.util.Constants;
import br.com.treinamento.util.MD5;

@Service("contactFacade")
@Scope("prototype")
public class CharactersFacade {
	
	public List<Character> getAllCharacters()
	{
		List<Character> list = new ArrayList<Character>();
		Client client = Client.create();
		
		Calendar cal = Calendar.getInstance();

		WebResource webResource = client
		   .resource(Constants.marvelUrl+"public/characters?ts="+cal.getTimeInMillis()+"&apikey="+Constants.publicKey+"&hash="+MD5.getMD5(cal.getTimeInMillis()+Constants.privateKey+Constants.publicKey));

		ClientResponse response = webResource.accept("application/json")
                   .get(ClientResponse.class);

		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject)parser.parse(output);
		JsonObject data = object.getAsJsonObject("data");
		JsonArray array = data.getAsJsonArray("results");
		
		
		for (int index = 0, total = array.size(); index < total; index++) {
			    final JsonObject jsonObject = array.get(index).getAsJsonObject();
			    Character character = new Character();
			                           
			    character.setName(jsonObject.get("name").getAsString());
			    character.setId(jsonObject.get("id").getAsInt());
			    character.setDescription(jsonObject.get("description").getAsString());
			    list.add(character);
			}

		return list;
		
	}
	
	public Character getCharacterByString(String character, List<Character> list)
	{
		Character charact = null;
		
		for(int i=0; i<list.size(); i++)
		{
			if(list.get(i).getName().toLowerCase().contains(character.toLowerCase()))
			{
				charact = list.get(i);
				i = list.size();
			}
		}
		
		return charact;
		
	}

}
