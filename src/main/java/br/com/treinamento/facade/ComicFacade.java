package br.com.treinamento.facade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.treinamento.model.Character;
import br.com.treinamento.model.Comic;
import br.com.treinamento.util.Constants;
import br.com.treinamento.util.MD5;

@Service("comicFacade")
@Scope("prototype")
public class ComicFacade {
	
	public List<Comic> getComicsByCharacter(int idCharacter)
	{
		List<Comic> list = new ArrayList<Comic>();
		Client client = Client.create();
		
		Calendar cal = Calendar.getInstance();

		WebResource webResource = client
		   .resource(Constants.marvelUrl+"public/characters/"+idCharacter+"/comics?ts="+cal.getTimeInMillis()+"&apikey="+Constants.publicKey+"&hash="+MD5.getMD5(cal.getTimeInMillis()+Constants.privateKey+Constants.publicKey));

		ClientResponse response = webResource.accept("application/json")
                   .get(ClientResponse.class);

		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject)parser.parse(output);
		JsonObject data = object.getAsJsonObject("data");
		JsonArray array = data.getAsJsonArray("results");
		
		
		for (int index = 0, total = array.size(); index < total; index++) {
				JsonObject jsonObject = array.get(index).getAsJsonObject();
			    Comic comic = new Comic();
			                           
			    comic.setId(jsonObject.get("id").getAsInt());
			    comic.setDescription(jsonObject.get("description").getAsString());
			    comic.setTitle(jsonObject.get("title").getAsString());
			    list.add(comic);
			}
		
		return list;
		
	}
	
	

}
