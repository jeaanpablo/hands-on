package br.com.treinamento.facade;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("creatorFacade")
@Scope("prototype")
public class CreatorFacade {

}
