package br.com.treinamento.model;

import br.com.treinamento.generic.model.GenericModel;

public class Comic extends GenericModel{
	
	private int id;
	private int digitalId;
	private String title;
	private String description;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDigitalId() {
		return digitalId;
	}
	public void setDigitalId(int digitalId) {
		this.digitalId = digitalId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
